//
//  AppDelegate.h
//  TableViewApp
//
//  Created by Lazar Jovicic on 7/5/18.
//  Copyright © 2018 Lazar Jovicic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

