//
//  TableViewCell.m
//  TableViewApp
//
//  Created by Lazar Jovicic on 7/5/18.
//  Copyright © 2018 Lazar Jovicic. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
