//
//  main.m
//  TableViewApp
//
//  Created by Lazar Jovicic on 7/5/18.
//  Copyright © 2018 Lazar Jovicic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
